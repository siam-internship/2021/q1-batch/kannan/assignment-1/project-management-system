<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ClientAdminController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//public routes


//protected routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    
//Admin

    Route::post('/admin-login', [AdminController::class, 'login']);
    Route::post('/admin-register', [AdminController::class, 'register']);
    Route::post('/admin-logout', [AdminController::class, 'logout']);
    Route::post('/admin-refresh', [AdminController::class, 'refresh']);
    Route::get('/admin-user-profile', [AdminController::class, 'userProfile']);
    Route::get('/admin', [AdminController::class, 'index']);
    Route::post('/admin', [AdminController::class, 'store']);
    Route::put('/admin/{id}', [AdminController::class, 'update']);
    Route::delete('/admin/{id}', [AdminController::class, 'destroy']);
    Route::get('/admin/{id}', [AdminController::class, 'show']);
    Route::get('/admin/search/{username}', [AdminController::class, 'search']);

//ClientAdmin

    Route::post('/client-login', [ClientAdminController::class, 'login']);
    Route::post('/client-register', [ClientAdminController::class, 'register']);
    Route::post('/client-logout', [ClientAdminController::class, 'logout']);
    Route::post('/client-refresh', [ClientAdminController::class, 'refresh']);
    Route::get('/client-user-profile', [ClientAdminController::class, 'userProfile']);
    Route::get('/manager', [ClientAdminController::class, 'index']);
    Route::post('/manager', [ClientAdminController::class, 'store']);
    Route::put('/manager/{id}', [ClientAdminController::class, 'update']);
    Route::delete('/manager/{id}', [ClientAdminController::class, 'destroy']);
    Route::get('/manager/{id}', [ClientAdminController::class, 'show']);
    Route::get('/manager/search/{username}', [ClientAdminController::class, 'search']);

//Manager

    Route::post('/manager-login', [ManagerController::class, 'login']);
    Route::post('/manager-register', [ManagerController::class, 'register']);
    Route::post('/manager-logout', [ManagerController::class, 'logout']);
    Route::post('/manager-refresh', [ManagerController::class, 'refresh']);
    Route::get('/manager-user-profile', [ManagerController::class, 'userProfile']);
    Route::get('/project', [ManagerController::class, 'index']);
    Route::post('/project', [ManagerController::class, 'store']);
    Route::delete('/project/{id}', [ManagerController::class, 'destroy']);
    Route::get('/project/{id}', [ManagerController::class, 'show']);
    Route::get('/project/search/{task_name}', [ManagerController::class, 'search']);
    Route::get('/project/filter/{task_status}', [ManagerController::class, 'filter']);

//Employee

    Route::post('/employee-login', [AuthController::class, 'login']);
    Route::post('/employee-logout', [AuthController::class, 'logout']);
    Route::post('/employee-refresh', [AuthController::class, 'refresh']);
    Route::get('/employee-user-profile', [AuthController::class, 'userProfile']);
    Route::get('/employee/find/{username}', [ManagerController::class, 'find']);
    Route::put('/taskupdate/{id}', [ManagerController::class, 'update']);
    
});