<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Admin extends Model
{
    
    protected $connection = 'mongodb';
    protected $collection = 'admin';
    use HasFactory;
    protected $fillable = [
        'company_name',
        'mobile_number',
        'email',
        'address',
        'username',
        'password',
    ];
}
