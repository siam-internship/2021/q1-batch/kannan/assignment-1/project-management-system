<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Manager extends Model
{
    
    protected $connection = 'mongodb';
    protected $collection = 'manager';
    use HasFactory;
    protected $fillable = [
        'project_name',
        'task_name',
        'start_date',
        'end_date',
        'employee',
        'task_status',
        'email',
        'username',
        'password',
    ];
}
