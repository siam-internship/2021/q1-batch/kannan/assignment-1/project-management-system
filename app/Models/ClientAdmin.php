<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class ClientAdmin extends Model
{
    
    protected $connection = 'mongodb';
    protected $collection = 'clientadmin';
    use HasFactory;
    protected $fillable = [
        'manager_name',
        'email',
        'mobile_number',
        'username',
        'password',
    ];
}
